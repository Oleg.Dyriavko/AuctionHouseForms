package com.example.demo.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;


import javax.validation.constraints.NotNull;

@Data
public class BuyerDto {

    @NotNull
    @Length(max=10)
    private int id;

    @NotNull
    @Length(max=10)
    private String firstName;

    @NotNull
    @Length(max=16)
    private String lastName;


}
