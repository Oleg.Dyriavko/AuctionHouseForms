package com.example.demo.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class SellerDto {


    private int id;

    @NotNull
    @Length(max = 10)
    private String name;
}
