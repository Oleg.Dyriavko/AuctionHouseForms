package com.example.demo.dto;


import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class BidDto {

    @NotNull
    @Length(max = 20)
    private int id;

    @NotNull
    @Length(max = 20)
    private int buyerId;

    @NotNull
    private int productId;

    @NotNull
    private int nominal;
    private boolean empty;

    public boolean isEmpty() {
        return empty;
    }
}
