package com.example.demo.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class ProductDto {


    private int id;

    @NotNull
    @Length(max = 16)
    private String city;

    @NotNull
    @Length(max = 20)
    private String street;

    @NotNull
    @Length(max = 5)
    private int number;

    @NotNull
    private int sellerId;

}
