package com.example.demo.validator;
import com.example.demo.dto.BidDto;
import com.example.demo.dto.BuyerDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class BidValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return BidDto.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        BidDto bidForm = (BidDto) target;
        int id = bidForm.getId();
        int buyerId = bidForm.getBuyerId();
        int productId = bidForm.getProductId();
        int nominal = bidForm.getNominal();

       /* if (bidForm.getId().length() >10){
            errors.rejectValue("firstName", "400", "Too long");
        }*/

        if (bidForm.isEmpty()) {
            errors.rejectValue("firstName", "400", "Field should not be empty.");
        }

    }
}
