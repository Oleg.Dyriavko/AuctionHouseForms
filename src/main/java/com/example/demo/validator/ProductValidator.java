package com.example.demo.validator;

import com.example.demo.dto.BuyerDto;
import com.example.demo.dto.ProductDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


@Component
public class ProductValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return ProductDto.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ProductDto productForm = (ProductDto) target;
        String city = productForm.getCity();
        if (!city.matches("/[^а-я ]+/msiu")) {
            errors.rejectValue("city", "400",
                    "City should contain nothing but Russian letters and space.");
        }
        if (city.isEmpty()) {
            errors.rejectValue("city", "400", "Field should not be empty.");
        }
    }

}
