package com.example.demo.validator;

import com.example.demo.dto.BuyerDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class BuyerValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return BuyerDto.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        BuyerDto buyerForm = (BuyerDto) target;
        String firstName = buyerForm.getFirstName();
        if (!firstName.matches("/[^а-я ]+/msiu")) {
            errors.rejectValue("firstName", "400",
                    "First name should contain nothing but Russian letters and space.");
        }
        if (firstName.isEmpty()) {
            errors.rejectValue("firstName", "400", "Field should not be empty.");
        }
    }
}
