package com.example.demo.validator;

import com.example.demo.dto.SellerDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class SellerValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return SellerDto.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        SellerDto sellerForm = (SellerDto) target;
        String name = sellerForm.getName();
        if (!name.matches("/[^а-я ]+/msiu")) {
            errors.rejectValue("Name", "400",
                    "Name should contain nothing but Russian letters and space.");
        }
        if (name.isEmpty()) {
            errors.rejectValue("Name", "400", "Field should not be empty.");
        }
    }
}
