package com.example.demo;

import com.example.demo.dto.BuyerDto;
import com.example.demo.dto.SellerDto;
import com.example.demo.validator.BidValidator;
import com.example.demo.validator.BuyerValidator;
import com.example.demo.validator.ProductValidator;
import com.example.demo.validator.SellerValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
public class MainRestController {
    @Autowired
    private BuyerValidator buyerValidator;
    private SellerValidator sellerValidator;
    private BidValidator bidValidator;
    private ProductValidator productValidator;

    public MainRestController(SellerValidator sellerValidator, BuyerValidator buyerValidator,
                              ProductValidator productValidator, BidValidator bidValidator) {
        this.sellerValidator = sellerValidator;
        this.buyerValidator = buyerValidator;
        this.productValidator = productValidator;
        this.bidValidator = bidValidator;
    }

    @InitBinder
    private void buyerBinder(WebDataBinder binder) {
        binder.setValidator(buyerValidator);
    }

    @InitBinder
    private void sellerBinder(WebDataBinder binder) {
        binder.setValidator(sellerValidator);
    }

    @InitBinder
    private void bidBinder(WebDataBinder binder) {
        binder.setValidator(bidValidator);
    }

    @InitBinder
    private void productBinder(WebDataBinder binder) {
        binder.setValidator(productValidator);
    }

    @RequestMapping(value = "/buyer", method = RequestMethod.POST)
    public ResponseEntity<String> getBuyerForm(@RequestBody @Valid BuyerDto buyerDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<String>(bindingResult.getAllErrors().get(0).getDefaultMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<String>(HttpStatus.OK);
    }
@RequestMapping(value = "/seller", method = RequestMethod.POST)
    public ResponseEntity<String> getSellerForm(@RequestBody @Valid SellerDto sellerDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<String>(bindingResult.getAllErrors().get(0).getDefaultMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<String>(HttpStatus.OK);
    }
@RequestMapping(value = "/bid", method = RequestMethod.POST)
    public ResponseEntity<String> getBidForm(@RequestBody @Valid BuyerDto bidDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<String>(bindingResult.getAllErrors().get(0).getDefaultMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<String>(HttpStatus.OK);
    }
@RequestMapping(value = "/product", method = RequestMethod.POST)
    public ResponseEntity<String> getProductForm(@RequestBody @Valid BuyerDto productDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<String>(bindingResult.getAllErrors().get(0).getDefaultMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<String>(HttpStatus.OK);
    }
}
