package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {
    private static final String successPage = "/static/success.html";

    @RequestMapping(value = "/seller", method = RequestMethod.GET)
    public String getForSeller(@RequestParam int id,
                               @RequestParam String name) {
        return successPage;
    }

    @RequestMapping(value = "/buyer", method = RequestMethod.GET)
    public String getForBuyer(@RequestParam int id,
                              @RequestParam String firstName,
                              @RequestParam String lastName) {
        return successPage;
    }

    @RequestMapping(value = "/bid", method = RequestMethod.GET)
    public String getForBid(@RequestParam int id,
                            @RequestParam int buyerId,
                            @RequestParam int productId,
                            @RequestParam int nominal) {
        return successPage;
    }

    @RequestMapping(value = "/product", method = RequestMethod.GET)
    public String getForProduct(@RequestParam int id,
                                @RequestParam String city,
                                @RequestParam String street,
                                @RequestParam int number,
                                @RequestParam int sellerId) {
        return successPage;
    }

}
